Entrar con el usuario de postgres a la DB y ejecutar la siguiente sentencia SQL
```bash
sudo -u postgres psql
ALTER USER postgres PASSWORD 'myPassword';
```

```bash
\q
```